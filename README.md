# Brotlidump #

When reading about the Brotli compressor, I was intrigued.
So, I wrote a Brotli dumper to see what is going on under the hood.

### What is this? ###

Brotlidump is a command line tool to experiment with the Brotli compressor.
Make sure you have the Python brotli library installed, try:
`pip install brotli`

The program allows to compress and decompress with the Brotli format,
but the real ting is the "-a" flag:
This gives a full description of literally every bit in the compressed Brotli file.
Now you can get an idea why this compressor is so ridiculously efficient!
Give "brotlidump -h" for a list of all the options.

### Contribution guidelines ###

Feel free to drop me a line.
The code could use a bit of cleanup.

### Who do I talk to? ###

Jurjen N. E. Bos